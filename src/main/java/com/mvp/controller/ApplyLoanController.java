package com.mvp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.mvp.exception.InvalidUserException;
import com.mvp.model.LoanDetail;
import com.mvp.model.LoanDetailDTO;
import com.mvp.model.SuccessResponse;
import com.mvp.service.LoanService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ApplyLoanController {

	@Autowired
	private LoanService loanService;

	/**
	 * apply loan to user
	 * @return "Loan Applied Successfully" if applies for first time
	 * @return "Loan is under evaluation" if applies for second time
	 * @param LoanDetail
	 * @param token
	 * @throws InvalidUserException
	 */

	@PostMapping("/applyloan")
	public ResponseEntity<SuccessResponse> applyLoan(@RequestHeader("Authorization") String token,
			@RequestBody LoanDetailDTO loanDetail) throws InvalidUserException {
		return ResponseEntity.ok(loanService.applyLoan(token,loanDetail));
		
	}

	@GetMapping("/viewloan/{username}")
	public ResponseEntity<List<LoanDetail>> viewStatus(@RequestHeader("Authorization") String token,
			@PathVariable String username){

		List<LoanDetail> updatedLoan = loanService.getLoanDetailsByName(username);
		return new ResponseEntity<List<LoanDetail>>(updatedLoan,HttpStatus.OK);

	}

}
