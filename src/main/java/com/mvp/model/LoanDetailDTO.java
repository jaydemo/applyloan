package com.mvp.model;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Component
public class LoanDetailDTO {

	private String userName;
	private String loanType;
	private long loanAmount;
	private LocalDate appliedDate;
	private double rateOfInterest;
	private String loanDuration;
	private String status;
}
