package com.mvp.service;

import java.util.List;

import com.mvp.exception.InvalidUserException;
import com.mvp.model.LoanDetail;
import com.mvp.model.LoanDetailDTO;
import com.mvp.model.SuccessResponse;

public interface LoanService {

	public boolean checkEligibility(String username, String typeOfLoan);

	public SuccessResponse applyLoan(String token, LoanDetailDTO loanDetail) throws InvalidUserException;

	public void approveLoan();

	public List<LoanDetail> getLoanDetailsByName(String name);

}
