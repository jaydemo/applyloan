package com.mvp.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.mvp.exception.InvalidUserException;
import com.mvp.feign.LoginClient;
import com.mvp.model.AuthResponse;
import com.mvp.model.LoanDetail;
import com.mvp.model.LoanDetailDTO;
import com.mvp.model.SuccessResponse;
import com.mvp.repo.LoanDetailRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoanServiceImpl implements LoanService {

	@Autowired
	private LoanDetailRepo repo;
	@Autowired
	private LoginClient login;

	// saves the loan detail in database
	@Override
	public SuccessResponse applyLoan(String token, LoanDetailDTO loanDetailDTO) throws InvalidUserException {
		AuthResponse auth = login.verifyToken(token).getBody();
		LoanDetail loanDetail = convertToLoanDetail(loanDetailDTO);
		if (Objects.nonNull(auth))
			if (!auth.isValid())
				throw new InvalidUserException("User is invalid");
			else {
				if (checkEligibility(loanDetail.getUserName(), loanDetail.getLoanType())) {
					repo.save(loanDetail);
				} else {
					return new SuccessResponse("Loan under evaluation", HttpStatus.OK, LocalDate.now());
				}

			}
		return new SuccessResponse("Loan applied successfully", HttpStatus.OK, LocalDate.now());

	}

	public LoanDetail convertToLoanDetail(LoanDetailDTO loanDetailDTO) {
		LoanDetail loanDetail = new LoanDetail();
		loanDetail.setUserName(loanDetail.getUserName());
		loanDetail.setAppliedDate(loanDetailDTO.getAppliedDate());
		loanDetail.setLoanAmount(loanDetailDTO.getLoanAmount());
		loanDetail.setLoanDuration(loanDetailDTO.getLoanDuration());
		loanDetail.setLoanType(loanDetailDTO.getLoanType());
		loanDetail.setRateOfInterest(loanDetailDTO.getRateOfInterest());
		return loanDetail;
	}

	@Override
	public boolean checkEligibility(String username, String typeOfLoan) {
		int nullcount = 0;
		int rejectedcount = 0;
		int count = repo.getDetailByUserName(username);
		if (count == 0) {
			return true;
		} else {
			List<LoanDetail> availableLoan = repo.getLoanStatusByName(username);
			for (LoanDetail loan : availableLoan) {
				nullcount = 0;
				rejectedcount = 0;
				if (loan.getLoanType().equalsIgnoreCase(typeOfLoan)) {
					if (loan.getStatus().equalsIgnoreCase("Rejected")) {
						rejectedcount++;
					}
					if (loan.getStatus().equalsIgnoreCase("null")) {
						nullcount++;
					}
				}
			}
			log.info(rejectedcount + "    " + nullcount);
			return nullcount - rejectedcount <= 0;
		}
	}

	@Override
	@Scheduled(initialDelay = 1000L, fixedDelay = 60 * 2000L)
	public void approveLoan() {
		List<LoanDetail> loans = repo.loandetails();
		for (LoanDetail loan : loans) {
			if (personalLoanCriteria(loan) || carLoanCriteria(loan) || propertyLoanCriteria(loan)
					|| jumboLoanCriteria(loan) || educationLoanCriteria(loan)) {
				LoanDetail updatedloan = loan;
				updatedloan.setStatus("Approved");
				repo.save(updatedloan);
			} else {
				LoanDetail updatedloan = loan;
				updatedloan.setStatus("Rejected");
				repo.save(updatedloan);
			}

		}
	}

	private boolean personalLoanCriteria(LoanDetail loanDetail) {
		return loanDetail.getLoanAmount() < 200000 && loanDetail.getLoanType().equalsIgnoreCase("PersonalLoan");
	}

	private boolean carLoanCriteria(LoanDetail loanDetail) {
		return loanDetail.getLoanAmount() < 500000 && loanDetail.getLoanType().equalsIgnoreCase("CarLoan");
	}

	private boolean propertyLoanCriteria(LoanDetail loanDetail) {
		return loanDetail.getLoanAmount() < 1000000 && loanDetail.getLoanType().equalsIgnoreCase("PropertyLoan");
	}

	private boolean jumboLoanCriteria(LoanDetail loanDetail) {
		return loanDetail.getLoanAmount() < 300000 && loanDetail.getLoanType().equalsIgnoreCase("JumboLoan");
	}

	private boolean educationLoanCriteria(LoanDetail loanDetail) {
		return loanDetail.getLoanAmount() < 200000 && loanDetail.getLoanType().equalsIgnoreCase("PersonalLoan");
	}

	@Override
	public List<LoanDetail> getLoanDetailsByName(String name) {
		List<LoanDetail> updatedLoan = repo.getLoanStatusByName(name);
		return updatedLoan;
	}

}
