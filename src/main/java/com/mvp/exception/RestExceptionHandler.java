package com.mvp.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.mvp.model.MessageResponse;

import lombok.extern.slf4j.Slf4j;

//global exception handler
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler {

	/**
	 * to handle unauthorized user
	 * @param UnauthorizedException
	 * @return ResponseEntity<MessageResponse>
	 */
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler({ UnauthorizedException.class})
	public ResponseEntity<?> handleUnauthorizedExceptions(Exception ex) {
		log.error("Unauthorized request...");
		return ResponseEntity.badRequest()
				.body(new MessageResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED));
	}
	/**
	 * to handle invalid user 
	 * @param UnauthorizedException
	 * @return ResponseEntity<MessageResponse>
	 */
	
		@ResponseStatus(HttpStatus.BAD_REQUEST)
		@ExceptionHandler({ InvalidUserException.class})
		public ResponseEntity<?> handleInvalidUserException(Exception ex) {
			log.error("Unauthorized request...");
			return ResponseEntity.badRequest()
					.body(new MessageResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED));
		}

	
	
}
