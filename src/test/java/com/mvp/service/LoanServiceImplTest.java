package com.mvp.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mvp.exception.InvalidUserException;
import com.mvp.feign.LoginClient;
import com.mvp.model.AuthResponse;
import com.mvp.model.LoanDetail;
import com.mvp.model.LoanDetailDTO;
import com.mvp.repo.LoanDetailRepo;

@SpringBootTest(classes = { LoanServiceImplTest.class })
public class LoanServiceImplTest {

	@Mock
	LoanDetailRepo repo;

	@InjectMocks
	LoanServiceImpl loanService;

	@InjectMocks
	InvalidUserException userExp;

	@Mock
	LoanDetailRepo detailRepo;

	@Mock
	LoginClient login;

	@Test
	public void test_applyLoan() throws InvalidUserException {
		LoanDetailDTO loanDetailDTO = new LoanDetailDTO("test123", "PersonalLoan", 12345L, LocalDate.of(2020, 1, 8),
				1.3, "5 Years", "null");
		String token = "tokentokentoken";
		when(login.verifyToken(token))
				.thenReturn(new ResponseEntity<AuthResponse>(new AuthResponse("test123", true), HttpStatus.OK));
		loanService.applyLoan(token, loanDetailDTO);
	}

	@Test
	public void test_applyLoanPersonalLoanFailure() throws InvalidUserException {
		LoanDetailDTO loanDetailDTO = new LoanDetailDTO("test123", "PersonalLoan", 300000, LocalDate.of(2020, 1, 8),
				1.3, "5 Years", "null");
		String token = "tokentokentoken";
		when(login.verifyToken(token))
				.thenReturn(new ResponseEntity<AuthResponse>(new AuthResponse("test123", true), HttpStatus.OK));
		loanService.applyLoan(token, loanDetailDTO);
	}

	@Test
	public void InvalidUserExceptionCheck() throws InvalidUserException {
		LoanDetailDTO loanDetailDTO = new LoanDetailDTO("test123", "PersonalLoan", 300000, LocalDate.of(2020, 1, 8),
				1.3f, "5 Years", "null");
		String token = "tokentokentoken";
		when(login.verifyToken(token))
				.thenReturn(new ResponseEntity<AuthResponse>(new AuthResponse("test123", false), HttpStatus.OK));
		assertThrows(InvalidUserException.class, () -> loanService.applyLoan(token, loanDetailDTO));

	}

	@Test
	public void test_checkEligibility() {
		List<String> availableLoan = new ArrayList<>();
		availableLoan.add("PersonalLoan");
		when(repo.getDetailByUserName(Mockito.anyString())).thenReturn(0);
		loanService.checkEligibility("name", "type");

		when(repo.getDetailByUserName(Mockito.anyString())).thenReturn(1);
		when(repo.getloanType(Mockito.anyString())).thenReturn(availableLoan);
		loanService.checkEligibility("name", "PersonalLoan");

		when(repo.getDetailByUserName(Mockito.anyString())).thenReturn(1);
		when(repo.getloanType(Mockito.anyString())).thenReturn(availableLoan);
		loanService.checkEligibility("name", "HomeLoan");

	}

	@Test
	public void test_approveLoan() {
		List<LoanDetail> loans = new ArrayList<>();
		loans.add(new LoanDetail(1, "test123", "PersonalLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"Approved"));
		loans.add(new LoanDetail(2, "test1234", "CarLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years", "null"));
		loans.add(new LoanDetail(3, "test1234", "PropertyLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		loans.add(
				new LoanDetail(4, "test1234", "JumboLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years", "null"));
		loans.add(new LoanDetail(5, "test1234", "EducationLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		loans.add(new LoanDetail(5, "test1234", "EducationLoan", 800001L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		loans.add(new LoanDetail(3, "test1234", "PropertyLoan", 1000003L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		loans.add(new LoanDetail(5, "test1234", "EducationLoan", 800000L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		when(repo.loandetails()).thenReturn(loans);
		loanService.approveLoan();

	}

	@Test
	public void test_getLoanStatus() {
		List<LoanDetail> list = new ArrayList<>();
		list.add(new LoanDetail(1, "test123", "PersonalLoan", 12345L, LocalDate.of(2020, 1, 8), 1.3f, "5 Years",
				"null"));
		when(repo.getLoanStatusByName(Mockito.anyString())).thenReturn(list);
		loanService.getLoanDetailsByName("test");
	}
}
