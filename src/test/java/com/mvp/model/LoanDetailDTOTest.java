package com.mvp.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class LoanDetailDTOTest {
	
	@Test
	void setter() {
		LoanDetailDTO detailDTO =new LoanDetailDTO();
		detailDTO.setUserName("test");
		detailDTO.setLoanType("personalLoan");
		detailDTO.setLoanAmount(150000);
		detailDTO.setAppliedDate(LocalDate.now());
		detailDTO.setRateOfInterest(5.5);
		detailDTO.setLoanDuration("5");
		detailDTO.setStatus("approved");
		assertNotNull(detailDTO);
	}
	
	@Test
	void getter() {
		LoanDetailDTO detailDTO = new LoanDetailDTO("test","personalLoan",150000,LocalDate.now(),5.5,"5","approved");
		detailDTO.getUserName();
		detailDTO.getLoanAmount();
		detailDTO.getLoanType();
		detailDTO.getAppliedDate();
		detailDTO.getRateOfInterest();
		detailDTO.getLoanDuration();
		detailDTO.getStatus();
		assertNotNull(detailDTO.toString());
	}
	
	@Test
	void equalsTestFailure() {
		LoanDetailDTO detailDTO = new LoanDetailDTO("test","personalLoan",150000,LocalDate.now(),5.5,"5","approved");
		LoanDetailDTO detailDTO2 = new LoanDetailDTO("test123","personalLoan",150000,LocalDate.now(),5.5,"5","approved");
		assertFalse(detailDTO.equals(detailDTO2));
		
	}


}
