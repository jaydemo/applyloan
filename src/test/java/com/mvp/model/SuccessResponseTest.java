package com.mvp.model;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class SuccessResponseTest {
	
	@Test
	void getterTest() {
		SuccessResponse response = new SuccessResponse("test",HttpStatus.ACCEPTED,LocalDate.now());
		response.getMessage();
		response.getStatus();
		response.getTimestamp();
	}
	
	@Test
	void setterTest() {
		SuccessResponse response = new SuccessResponse();
		response.setMessage("test");
		response.setStatus(HttpStatus.OK);
		response.setTimestamp(LocalDate.now());
	}
}
