package com.mvp.exceptions;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.mvp.exception.InvalidUserException;
import com.mvp.exception.RestExceptionHandler;
import com.mvp.exception.UnauthorizedException;


@SpringBootTest(classes = {RestExceptionHandlerTest.class})
public class RestExceptionHandlerTest {

	@Test
	void test_handleUnauthorizedExceptions() {
		UnauthorizedException ex = new UnauthorizedException("Test_handleUnauthorizedException");
		RestExceptionHandler restExceptionHandler = new RestExceptionHandler();
		restExceptionHandler.handleUnauthorizedExceptions(ex);
	}
	
	@Test
	void test_handleInvalidUserException() {
		InvalidUserException ex = new InvalidUserException("Test_handleUnauthorizedException");
		RestExceptionHandler restExceptionHandler = new RestExceptionHandler();
		restExceptionHandler.handleUnauthorizedExceptions(ex);
	}
	
	
	
}
